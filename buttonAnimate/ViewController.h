//
//  ViewController.h
//  buttonAnimate
//
//  Created by Ankur Chauhan on 11/05/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{


    IBOutlet UIButton *fbBtn;
    IBOutlet UIButton *gmailBtn;
    IBOutlet UIButton *twBtn;
    IBOutlet UIButton *PiBtn;
    IBOutlet UIButton *HiBtn;


}
- (IBAction)shareBtnClicked:(UIButton *)sender;
- (IBAction)fbBtnClicked:(UIButton *)sender;
- (IBAction)gmailBtnClicked:(UIButton *)sender;
- (IBAction)piBtnClicked:(UIButton *)sender;
- (IBAction)twBtnClicked:(UIButton *)sender;


@end
