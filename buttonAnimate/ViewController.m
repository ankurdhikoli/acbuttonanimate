//
//  ViewController.m
//  buttonAnimate
//
//  Created by Ankur Chauhan on 11/05/14.
//  Copyright (c) 2014 company. All rights reserved.
//

#import "ViewController.h"
#define DEGREES_TO_RADIANS(d) (d * M_PI / 180)

@interface ViewController (){
    bool isOpened;
    bool isSpining;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareBtnClicked:(UIButton *)sender {
    
    if (isOpened) {
        isOpened=NO;
        sender.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(0));

        [self closeButtons];
    }
    else{
        isOpened=YES;
        [self openButtons];
        sender.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-45));

    }
}

-(void)openButtons{
    [self StartSpin];
    HiBtn.userInteractionEnabled=NO;
    [UIView animateWithDuration:1.0f
                          delay:0
                        options:0
                     animations:^{
                         [fbBtn setCenter:CGPointMake(160, HiBtn.center.y-75)];
                         [gmailBtn setCenter:CGPointMake(160, HiBtn.center.y-150)];
                         [twBtn setCenter:CGPointMake(160, HiBtn.center.y-225)];
                         [PiBtn setCenter:CGPointMake(160, HiBtn.center.y-300)];

                     }
                     completion:^(BOOL finished){
                         [self StopSpin];
                         HiBtn.userInteractionEnabled=YES;


                     }];
    
}

-(void)closeButtons{
    [self StartSpin];
    HiBtn.userInteractionEnabled=NO;

    [UIView animateWithDuration:1.0f
                          delay:0
                        options:0
                     animations:^{
                         [fbBtn setCenter:CGPointMake(160, HiBtn.center.y)];
                         [gmailBtn setCenter:CGPointMake(160, HiBtn.center.y)];
                         [twBtn setCenter:CGPointMake(160, HiBtn.center.y)];
                         [PiBtn setCenter:CGPointMake(160, HiBtn.center.y)];


                     }
                     completion:^(BOOL finished){
                         [self StopSpin];
                         HiBtn.userInteractionEnabled=YES;

                         
                     }];

}
#pragma mark Start/StopSpining

-(void)StartSpin{
    isSpining = TRUE;
    [self Spin];
}

-(void)StopSpin{
    isSpining = FALSE;
}


-(void)Spin{

[UIView animateWithDuration:0.1 delay:0 options: UIViewAnimationOptionCurveLinear animations:^{
    PiBtn.transform = CGAffineTransformRotate(PiBtn.transform, M_PI);
    twBtn.transform = CGAffineTransformRotate(twBtn.transform, M_PI);
    gmailBtn.transform = CGAffineTransformRotate(gmailBtn.transform, M_PI);
    fbBtn.transform = CGAffineTransformRotate(fbBtn.transform, M_PI);

}  completion:^(BOOL finished) {
    if (isSpining)
        [self Spin];
}];
}



- (IBAction)fbBtnClicked:(UIButton *)sender{

}
- (IBAction)gmailBtnClicked:(UIButton *)sender{
}
- (IBAction)piBtnClicked:(UIButton *)sender{
}
- (IBAction)twBtnClicked:(UIButton *)sender{
}
@end
